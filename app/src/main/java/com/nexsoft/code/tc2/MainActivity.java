package com.nexsoft.code.tc2;

import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.util.Log;
import android.view.Display;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.nexsoft.code.tc2.dblocal.DbChart;
import com.nexsoft.code.tc2.notimportant.DemoBase;

import java.util.ArrayList;

//AppCompatActivity
public class MainActivity extends DemoBase {

    private PieChart mChart;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        deleteAndSave();

        mChart = (PieChart) findViewById(R.id.chartHP);
        mChart.setBackgroundColor(Color.WHITE);

        moveOffScreen();

        mChart.setUsePercentValues(true);
        mChart.getDescription().setEnabled(false);

        mChart.setCenterTextTypeface(mTfLight);
        //mChart.setCenterText(generateCenterSpannableText());

        mChart.setDrawHoleEnabled(true);
        mChart.setHoleColor(Color.WHITE);

        mChart.setTransparentCircleColor(Color.WHITE);
        mChart.setTransparentCircleAlpha(110);

        mChart.setHoleRadius(28f); //58f
        mChart.setTransparentCircleRadius(41f); //61f

        mChart.setDrawCenterText(true);

        mChart.setRotationEnabled(false);
        mChart.setHighlightPerTapEnabled(true);

        mChart.setMaxAngle(180f); // HALF CHART
        mChart.setRotationAngle(180f);
        mChart.setCenterTextOffset(0, -20);

        setData(5, 100);

        mChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);

        Legend l = mChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);

        // entry label styling
        mChart.setEntryLabelColor(Color.WHITE);
        mChart.setEntryLabelTypeface(mTfRegular);
        mChart.setEntryLabelTextSize(12f);


    }

    private void deleteAndSave() {
        DbChart db = new DbChart(getApplicationContext());
        db.open();
        db.deleteAll(0);
        db.close();

        //SaveDataToLocal
        saveChart(10.0,"C1");
        saveChart(40.0,"C2");
        saveChart(50.0,"C3");

    }

    public void saveChart(double number, String title) {
        DbChart db = new DbChart(getApplicationContext());
        db.open();
        db.insertHalfChart(number, title);
        db.close();
        Toast.makeText(MainActivity.this,"Data Saved!", Toast.LENGTH_LONG).show();
    }

    private void setData(int count, float range) {

        ArrayList<PieEntry> values = new ArrayList<PieEntry>();

//        for (int i = 0; i < count; i++) {
//            values.add(new PieEntry((float) ((Math.random() * range) + range / 5), mParties[i % mParties.length]));
//        }


        DbChart db = new DbChart(getApplicationContext());
        db.open();
        Cursor cur = db.getAllLineChart();
        if(cur.getCount()!=0){
            DbChart db2 = new DbChart(getApplicationContext());
            db2.open();
            Cursor cur2 = db.getAllLineChart();
            if (cur2 != null) {
                if (cur2.moveToFirst()) {
                    if (cur2.getCount() != 0) {
                        do {

                            values.add(new PieEntry((float) (cur2.getDouble(1)), cur2.getString(2)));
                            Log.d("DbChart", cur2.getString(0) +". "+ cur2.getString(1)+", "+cur2.getString(2));

                        } while (cur2.moveToNext());
                    } else {
                    }
                }
            } else {
            }
            db.close();

        }else {
        }
        db.close();

//        values.add(new PieEntry((float) (50.0), "C1"));
//        values.add(new PieEntry((float) (30.0), "C2"));
//        values.add(new PieEntry((float) (20.0), "C3"));

        PieDataSet dataSet = new PieDataSet(values, "The Results");
        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);

        dataSet.setColors(ColorTemplate.MATERIAL_COLORS);
        dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);
        data.setValueTypeface(mTfLight);
        mChart.setData(data);

        mChart.invalidate();
    }

    private SpannableString generateCenterSpannableText() {

//        SpannableString s = new SpannableString("Chart Half Pie\ndeveloped by Philipp Jahoda");
//        s.setSpan(new RelativeSizeSpan(1.7f), 0, 14, 0);
//        s.setSpan(new StyleSpan(Typeface.NORMAL), 14, s.length() - 15, 0);
//        s.setSpan(new ForegroundColorSpan(Color.GRAY), 14, s.length() - 15, 0);
//        s.setSpan(new RelativeSizeSpan(.8f), 14, s.length() - 15, 0);
//        s.setSpan(new StyleSpan(Typeface.ITALIC), s.length() - 14, s.length(), 0);
//        s.setSpan(new ForegroundColorSpan(ColorTemplate.getHoloBlue()), s.length() - 14, s.length(), 0);
//        return s;
        return null;
    }

    private void moveOffScreen() {

        Display display = getWindowManager().getDefaultDisplay();
        int height = display.getHeight();  // deprecated

        int offset = (int)(height * 0.65); /* percent to move */

        RelativeLayout.LayoutParams rlParams =
                (RelativeLayout.LayoutParams)mChart.getLayoutParams();
        rlParams.setMargins(0, 0, 0, -offset);
        mChart.setLayoutParams(rlParams);
    }

}
