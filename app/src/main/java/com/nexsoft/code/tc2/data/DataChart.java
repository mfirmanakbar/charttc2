package com.nexsoft.code.tc2.data;

/**
 * Created by firmanmac on 3/1/17.
 */

public class DataChart {
    int id;
    float number;
    String title;

    public DataChart() {
    }

    public DataChart(int id, float number, String title) {
        this.id = id;
        this.number = number;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getNumber() {
        return number;
    }

    public void setNumber(float number) {
        this.number = number;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
