package com.nexsoft.code.tc2.dblocal;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by firmanmac on 3/1/17.
 */

public class DbOpenHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "myappdb.db";

    private static final String TABLE_CELENGAN_CREATE = "CREATE TABLE tb_line_charts (id INTEGER PRIMARY KEY AUTOINCREMENT, number REAL, title TEXT)";

    public DbOpenHelper(Context context, String name, CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CELENGAN_CREATE);

        //Contoh Insert
        //		ContentValues cv = new ContentValues();
        //		cv.put("id", "");
        //      cv.put("angka", "12f, 4");
        //		db.insert("tb_line_chart", null, cv);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVer, int newVer) {
        db.execSQL("DROP TABLE IF EXISTS tb_line_charts");
        onCreate(db);
    }
}
