package com.nexsoft.code.tc2.dblocal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by firmanmac on 3/1/17.
 */

public class DbChart {
    private SQLiteDatabase db;
    private final Context con;
    private final DbOpenHelper dbHelper;

    public DbChart(Context con) {
        this.con = con;
        dbHelper = new DbOpenHelper(this.con, "", null, 0);
    }

    public void open() {
        db = dbHelper.getWritableDatabase();
    }

    public void close() {
        db.close();
    }

    public long insertHalfChart(double number, String title) {
        ContentValues newValues = new ContentValues();
        newValues.put("number", number);
        newValues.put("title", title);
        return db.insert("tb_line_charts", null, newValues);
    }

    public long deleteAll(int id) {
        return db.delete("tb_line_charts", "id != " + id, null);
    }

    public Cursor getAllLineChart(){
        return db.rawQuery("SELECT * FROM tb_line_charts", null);
    }


}
